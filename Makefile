all: index.html course.pdf

index.html: course.adoc
	    asciidoctor $< -o index.html

course.pdf: course.adoc
	    asciidoctor-pdf $< 
